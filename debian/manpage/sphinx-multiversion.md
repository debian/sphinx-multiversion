TH  SPHINX  MULTIVERSION  "1"   "AGO  2022"  "SPHINX MULTIVERSION
"0.2.4‐1""Sphinx extension for building self‐hosted"

NAME
       spninx‐multiversion ‐ Sphinx extension for building  self‐hosted versioned docs.

SYNOPSIS
       sphinx‐multiversion [POSITION] [argument]

DESCRIPTION
       sphinx‐multiversion  ‐ Sphinx extension for building self‐hosted versioned documenta‐
       tion. Aims to provide a clean implementation that tries to avoid messing with Sphinx
       internals as much as possible.

USAGE

   a) sphinx‐multiversion [‐h] [‐d]

   b) sphinx‐multiversion [‐c PATCH] [‐C]

   c) sphinx‐multiversion [‐D setting=value][‐‐dump‐metadata]

   d) sphinx‐multiversion [sourcedir] [outputdir] [filenames ...]

OPTIONS

   a) sphinx‐multiversion ‐h
      show this help message and exit

   b) sphinx‐multiversion ‐c PATH
      path where configuration file (conf.py) is located (default: same as SOURCEDIR)

   c) sphinx‐multiversion ‐C
      use no config file at all, only ‐D options

   d) sphinx‐multiversion ‐D setting=value
      override a setting in configuration file

   e) sphinx‐multiversion ‐‐dump‐metadata
      dump generated metadata and exit

CONFIGURATION
       sphinx‐multiversion reads your Sphinx conf.py file for configuration. As usual,
       you can also override certain options by using ‐D var=value on the command line.
       This is what the default configuration looks like:

   a) Whitelist pattern for tags (set to None to ignore all tags)
      smv_tag_whitelist = \r\/’\^.*\$\/’

   b) Whitelist pattern for branches (set to None to  ignore  all branches)
      smv_branch_whitelist = r\/’\^.*\$\/’

   c)  Whitelist pattern for remotes (set to None to use local branches only)
      smv_remote_whitelist = None

   d) Pattern for released versions
      smv_released_pattern = r\/’\^tags/.*\$\/’

   e) Format for versioned output directories  inside  the  build directory 
      smv_outputdir_format = \/’{ref.name}\/’

   f) etermines whether remote  or local git branches/tags are preferred if their
      output dirs conflict
      smv_prefer_remote_refs = False

 Note:
       You can check which tags/branches are matched by running sphinx‐multiversion with
       the ‐‐dump‐metadata flag. Branches or tags that don’t contain both the sphinx source
       directory and the conf.py file will be skipped automatically. You can override all of
       these values inside your conf.py.

EXAMPLES

   1. Tag/Branch/Remote whitelists

   Tags, Branches and Remotes are included by Regular Expressions
Here are  some  exam‐ ples:

   a) smv_tag_whitelist = r\/’\^.*\$\/’ #Include all tags

   b) smv_tag_whitelist = r\/’\^v+.+\$\/’ # Include tags like \"v2.1\"

   c) smv_branch_whitelist = r\/’\^.*\$\/’ #Include all branches

   d) smv_branch_whitelist = r\/’\^(?!master).*\$\/’ #Include all branches except \"master\"

   e) smv_remote_whitelist = None Only #use local branches

   f) smv_remote_whitelist = r\/’\^.*\$\/’  #Use branches from all remotes

   g) smv_remote_whitelist = r\/’\^(origin|upstream)\$\/’ #Use branches from origin and upstream

   Note:
   To list values to match, you can use git branch, git  tag  and git remote

   2. Release Pattern

   A  Regular Expression is used to determine if a version of the
   documentation has been released or if it’s  a  development  version. To allow more
   flexibility, the regex is evaluated over the full refname.

   Here are some examples:

   a) smv_released_pattern = \r\/’\^tags/.*\$\/’ #Tags only

   b) smv_released_pattern = \r\/’\^heads/+.+\$\/’ #Branches like \"2.1\"

   c) smv_released_pattern = \r\/’\^(tags/.*|heads/+.+)\$\/’ # Branches like \"2.1\" and all tags

   d) smv_released_pattern = \r\/’\^(heads|remotes/[\^/]+)/(?!:master).*\$\/’
      Everything except master branch

    Note:
    To list all refnames, you can use:
    git for‐each‐ref ‐‐format \"%(refname)\" | sed \/’s/\^refs//g\/’

   3. Output Directory Format

   Each version  will  be built into a separate subdirectory of the Sphinx output direc‐
   tory. The smv_outputdir_format setting determines the directory  structure for the
   subdirectories.  It  is  a  new‐style Python formatting string with two parameters ‐ ref
   and config.

   Here are some examples:
   a) smv_outputdir_format = \/’{ref.name}\/’ #Use the branch/tag name

   b) smv_outputdir_format = \/’{ref.commit}\/’ # Use the commit hash

   c) smv_outputdir_format = \/’{ref.commit:.7s}\/’ # Use the commit hash truncated to 7 characters

   d)  smv_outputdir_format = \/’{ref.refname}\/’ #Use the full refname

   e) smv_outputdir_format = \/’{ref.source}/{ref.name}\/’ # Equivalent to the previous example

   f) smv_outputdir_format = \/’versions/{config.release}\/’ #Use \"versions\" as parent directory
      and the \"release\" variable from conf.py

   g) smv_outputdir_format = \/’{config.version}/{ref.name}\/’ #Use  the  version  from conf.py as
      parent directory and the branch/tag name as subdirectory

   4. Overriding Configuration Variables

   You can override  configuration  variables  the  same  way  as you’re used to with sphinx‐
   build. Since sphinx‐multiversion copies the branch data into a temporary directory and
   builds them there while leaving the current working  directory unchanged,  relative
   paths in your conf.py will refer to the path of the version you’re building from,
   not the path of the version  you  are  trying  to  build documentation for.

   Sometimes  it might be necessary to override the configured path via a command line
   override. sphinx‐multiversion  allows   you   to insert placeholders into your override
   strings  that  will automatically be replaced with the correct value for  the  version
   you’re building the documentation for.

   Here’s   an   example  for  the  exhale   extension:   sphinx‐multiversion docs build/html ‐D
   \exhale_args.containmentFolder=\$ 
    Note:

   Make  sure  to  enclose  the  override string in single quotes (\/’) to prevent the shell
   from  treating  it as an environment variable and replacing it before it’s passed to
   sphinx‐multiversion. To  see a list of available placeholder names and their values
   for each  version  you can use the ‐‐dump‐metadata flag.

   where to find in this manual ’ read ´.

SEE ALSO
    Have a look at PyFormat for information how to use  new‐style Python formatting.

AUTHOR
   Jan Holthuis jan.holthuis@ruhr‐uni‐bochum.de ⟨mailto:jan.holthuis@ruhr‐uni‐
   bochum.de⟩ Based on https://github.com/Holzhaus/sphinx‐ multiversion/blob/mas‐
   ter/README.md.

   Josenilson Ferreira da Silva  nilsonfsilva@hotmail.com ⟨mailto:nilsonfsilva@hot‐
   mail.com⟩ Created this man‐page on markdown, and used go‐md2man to generate the man‐
   ual file.
